<?php

use App\User;
use App\Post;
use App\Tag;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PostTest extends TestCase
{
    
    use DatabaseTransactions;
    use WithoutMiddleware;

    /** @test */
    function an_admin_creates_a_post()
    {
        $_SERVER['HTTP_HOST'] = 'kevmul.dev';
        // $user = factory(User::class)->create(); 
        $this->visit('/admin/post/create')
             ->type('New Post', 'title')
             ->type('Body copy', 'body')
             ->attach(public_path() . '/img/post/2016/jan/1452303271__Dandelion.jpg', 'featured')
             ->press('Create Post')
             ->seePageIs('/admin/post')
             ->see('new-post');
    }

    function test_post_has_a_feature_image()
    {
        // factory(Post::class, 3)->create();
    }
}
