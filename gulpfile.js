var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('styles.sass', false, {
    	indentedSyntax: true
		})
		.sass('admin_styles.sass', false, {
			indentedSyntax: true
		})
	 .scripts([
	 		'./node_modules/jquery/dist/jquery.js',
	 		'./node_modules/jquery-mousewheel/jquery.mousewheel.js',
	 		'getscrollin.js',
	 		'imageSizer.js'
	 	], './public/js/public.js')
	 .scripts([
	 		'./node_modules/jquery/dist/jquery.js',
	 		'libs/select2.min.js',
	 		'admin.js' 
	 	], './public/js/admin.js');
});
