<?php

namespace App;

use Image;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Featured {
	public $time;
	public $name;
	public $extension;
    public $directory = '';
	
	private $yearMonth;
	private $image_dir = '/img/post/';

	protected $thumbnailHeights = [
		500, 300, 50
	];


	/**
	 * Set up the class
	 */
	function __construct()
	{
		$this->time = str_replace(':', '_', Carbon::now());
		$this->yearMonth = strtolower(date('Y/M'));
		$this->setup();
	}


	/**
	 * Set up all directories
	 * 
	 */
	public function setUp()
	{
		$this->setDirectory();
    	$this->makeDirectory();
    }


	/**
	 * Persist file to directory
	 * 
	 * @param  UploadedFile $file 
	 * @return string Name
	 */
	public function persist($file)
	{
		$this->createName($file);

		$this->makeMultiResolutionImages($file);
	}
 

 	/**
 	 * Create the name of Featured image
 	 * 
 	 * @param  UploadedFile $file 
 	 * @return String Name
 	 */
	private function createName($file)
	{
		$this->extension = '.' . $file->getClientOriginalExtension();
		$basename = str_replace($this->extension, '', $file->getClientOriginalName());
    	$this->name = str_replace(' ', '_', $this->time . '__' . $basename);
	}


	/**
	 * Set up the Directory for persisting the file
	 * 
	 */
	private function setDirectory()
	{
		$this->directory = $this->image_dir . $this->yearMonth . '/';
	}


	/**
	 * Create the directories for files to save
	 * 
	 * @return bool 
	 */
	private function makeDirectory()
	{
		if(!is_dir(public_path() . $this->directory))
    	{
    		mkdir(public_path() . $this->directory, 0777, true);
    	}
	}


	public function makeMultiResolutionImages($featured)
	{
		$img = Image::make($featured)
    		  ->save(public_path() . $this->directory . $this->name . $this->extension);

    	foreach($this->thumbnailHeights as $height)
    	{
            $img->resize(null, $height, function ($constraint)
            {
            	$constraint->aspectRatio();
            })
            ->save(public_path() . $this->directory . $this->name . '--' . $height . $this->extension);
	    }

	    $img->destroy();
	}


    /**
     * Gets the value of directory.
     *
     * @return mixed
     */
    public function getDirectory()
    {
        return $this->directory;
    }

    static function destroy($file, $extension=".jpg")
    {
    	$featured = new self;
        if($file)
        {
            if(is_file($original = public_path() . $file . $extension)){
                unlink($original);
                foreach($featured->thumbnailHeights as $size){
                    if(is_file($link = public_path() . $file . '--' . $size . $extension))
                    {
                        unlink($link); 
                    }
                }
            }
        }
        return true;
    } 

}
