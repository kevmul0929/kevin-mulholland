<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Index
Route::get('/', 'PostsController@index');

// Posts
Route::resource('post', 'PostsController');

// Tags
Route::get('tag/{id}', 'TagsController@show');

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Admin
Route::get('admin', 'AdminsController@index');

// Admin Posts
Route::group(['middleware' => 'super_user'], function() {
	Route::resource('admin/post', 'AdminPostsController');
});

// Admin Tags
Route::resource('admin/tag', 'AdminTagsController');

// Admin Images
Route::resource('admin/media', 'AdminMediaController');

Route::get('/.well-known/acme-challenge/ig8GQq9qikF3fPoPza2myl-FwOAktSj4GM5GkNW4cuw', function() {
    echo 'ig8GQq9qikF3fPoPza2myl-FwOAktSj4GM5GkNW4cuw.A7TdW9xtuyCQ97iltGp9etjr2uorn0mBz_EmPM0Qb2A';
    return;
});
Route::get('/.well-known/acme-challenge/XpHD6p1EfPc4GsrpWOcIRdrWL9T0W_HUGk0w-b-xECc', function () {
    echo 'XpHD6p1EfPc4GsrpWOcIRdrWL9T0W_HUGk0w-b-xECc.A7TdW9xtuyCQ97iltGp9etjr2uorn0mBz_EmPM0Qb2A';
    return;
});
