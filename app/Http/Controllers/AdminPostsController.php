<?php

namespace App\Http\Controllers;

use Schema;
use App\Tag;
use App\Media;
use App\Post;
use App\Attachment;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Http\Controllers\Controller;

class AdminPostsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::latest('updated_at')
                ->with('featured')
                ->get();
        // $columns = Schema::getColumnListing('posts'); // For creating table dynamiclly
        return view('post.admin.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::lists('name', 'id');

        $featured = Media::wherenull('post_id')->get()->lists('name', 'id');

        $images = Media::wherenull('attachment_id')->get();

        return view('post.admin.create', compact('tags', 'featured', 'images'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        // dd($request->input('attachments'));
        $post = new Post($request->all());

        $post->createPost($post);

        $post->setFeatured($request->input('featured'));

        if($request->input('attachments')){
            $post->setAttachments($request->input('attachments'));
        }

        $post->syncTags($request->input('tag_list'));

        return redirect('admin/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Post::whereSlug($slug)->firstOrFail();

        return view('post.admin.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $post = Post::whereSlug($slug)->firstOrFail();

        $tags = Tag::lists('name', 'id');

        $featured = Media::wherenull('post_id')
            ->orWhere('post_id', '=', $post->id)
            ->get()
            ->lists('name', 'id');

        $images = Media::wherenull('attachment_id')
            ->orWhere('attachment_id', '=', $post->id)
            ->get();

        return view('post.admin.edit', compact('post', 'tags', 'images', 'featured'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $slug)
    {
        $post = Post::where('slug', '=', $slug)->firstOrFail();

        $post->updated_at = Carbon::now();

        $post->updateFeatured($request->input('featured'));
        
        $post->update($request->all());

        if($request->input('attachments')){
            $post->syncAttachments($request->input('attachments'));
        }

        $post->syncTags($request->input('tag_list'));
        return redirect('admin/post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $post = Post::where('slug', $slug)->firstOrFail();
        
        $featured = Media::where('post_id', '=', $post->id)->first();
        $featured->post_id = null;
        $featured->save();

        $attachments = Media::where('attachment_id', '=', $post->id)->get();
        foreach($attachments as $attachment)
        {
            $attachment->attachment_id = null;
            $attachment->save();
        }

        $post->destroyPost();
        return redirect('/admin/post');
    }
}
