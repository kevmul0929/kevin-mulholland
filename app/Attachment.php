<?php

namespace App;

use Image;
use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{

	public $table = 'post_attachemts';

	/**
	 * Fillable inputs
	 * 
	 * @var array
	 */
    public $fillable = [
    	'path'
    ];

    /**
     * Directory for attached files
     * 
     * @var string
     */
    private $image_dir = '/img/attachment/';


    /**
     * Create Attachment
     * 
     * @param  PostRequest $request 
     * @return App\Attachment          
     */
    public static function createAttachment($attachments, $postid)
    {
        foreach($attachments as $image)
        {
            if($image != null)
            {
            	$attachment = new static;
                $attachment->post_id = $postid;
            	$attachment->moveAttachment($image);
                $attachment->save();
    	   }
        }
    }

    private function moveAttachment($attachment)
    {
        // dd($attachment);
    	$basename = $attachment->getClientOriginalName();
    	$directory = $this->image_dir . strtolower(date('Y/M')) . '/';
    	$name = str_replace(' ', '_', time() . '__' . $basename);
        if(!is_dir(public_path() . $directory))
        {
            mkdir(public_path() . $directory, 0777, true);
        }
        Image::make($attachment)
              ->save(public_path() . $directory . $name);
        $this->path = $directory.$name;
    }


    public static function destroyAttachments($postid)
    {
        $attachments = Attachment::where('post_id', '=', $postid)->get();
        foreach($attachments as $attachment)
        {
            if(is_file($file = public_path() . $attachment->path))
            {
                unlink($file);
            }
        }
    }

    /**
     * An attachment belongs to a Post
     * 
     * @return \Illuminate\Database\Eloquent\BelongsTo 
     */
    public function post()
    {
    	return $this->belongsTo('App\Post');
    }
}
