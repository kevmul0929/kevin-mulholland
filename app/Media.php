<?php

namespace App;

use Carbon\Carbon;
use Image;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{

	protected $table = 'media';

	/**
	 * Allow which feilds are able to be filled in
	 * 
	 * @var array
	 */
	protected $fillable = [
		'name'
	];

	public $time;
    public $directory = '';
    public $fileName;
    public $extension;
	
	private $yearMonth;
	private $image_dir = '/img/image/';

	protected $thumbnailSizes = [
		'wide' => [
			'height' 	=> null, 
			'width' 	=> 1000],
		'large' => [
			'height'	=> 500, 
			'width' 	=> null],
		'medium' => [
			'height'	=> 300, 
			'width'		=> null],
		'small' => [
			'height'	=> null, 
			'width'		=> 80],
	];


	/**
	 * Set up the class
	 */
	function __construct()
	{
		$this->time = str_replace(':', '-', Carbon::now());
		$this->yearMonth = strtolower(date('Y/M'));
		$this->setup();
	}


	/**
	 * Set up all directories
	 * 
	 */
	public function setUp()
	{
		$this->setDirectory();
    	$this->makeDirectory();
    }


    /**
	 * Set up the Directory for persisting the file
	 * 
	 */
	private function setDirectory()
	{
		$this->directory = $this->image_dir . $this->yearMonth . '/';
	}


	/**
	 * Create the directories for files to save
	 * 
	 * @return bool 
	 */
	private function makeDirectory()
	{
		if(!is_dir(public_path() . $this->directory))
    	{
    		mkdir(public_path() . $this->directory, 0777, true);
    	}
	}


	/**
	 * Persist file to directory
	 * 
	 * @param  UploadedFile $file 
	 * @return self
	 */
	public static function persist($request)
	{
		$media = new static;

		$media->name = $request['name'];

		$media->createName($request['file']);

		$media->makeMultiResolutionImages($request['file']);

		$media->path = $media->directory . $media->fileName . $media->extension;
		
		return $media;
	}
 

 	/**
 	 * Create the name of Featured image
 	 * 
 	 * @param  UploadedFile $file 
 	 * @return String Name
 	 */
	private function createName($file)
	{
		$this->extension = '.' . $file->getClientOriginalExtension();
		$basename = str_replace($this->extension, '', $file->getClientOriginalName());
    	$this->fileName= str_replace(' ', '_', $this->time . '__' . $basename);
	}


	/**
	 * Create multiple sized images from source
	 * 
	 * @param  $file 
	 * @return [type]     
	 */
	public function makeMultiResolutionImages($file)
	{
		$img = Image::make($file->getRealPath())
    		  ->save(public_path() . $this->directory . $this->fileName . $this->extension);

    	foreach($this->thumbnailSizes as $thumbnail => $value)
    	{
            $img->resize($value['width'], $value['height'], function ($constraint)
            {
            	$constraint->aspectRatio();
            })
            ->save(public_path() . $this->directory . $this->fileName . '--' . $thumbnail . $this->extension);
	    }

	    $img->destroy();
	}

	/**
	 * Destroy the files
	 * 
	 * @param  $file      
	 * @param  string $extension 
	 * @return self            
	 */
	public function destroyMedia()
    {
    	if(file_exists(public_path() . $this->path))
        {
            if(is_file($original = public_path() . $this->path))
            {
                unlink($original);
                $exploded = $this->explode();
                foreach($this->thumbnailSizes as $thumbnail => $value)
                {
                    if(is_file($link = public_path() . $exploded[0] . '--' . $thumbnail . '.' . $exploded[1]))
                    {
                        unlink($link); 
                    }
                }
            }
        }
        return $this;
    } 

	/**
	 * return all posts this belongs to
	 * 
	 * @return Post $post
	 */
    // public function posts()
    // {
    // 	return $this->belongsToOne('App\Post');
    // }

    protected function explode()
    {
    	return explode('.', $this->path);
    }

    public function size($size = null)
    {
    	if($size)
    	{
    		$size = '--' . $size;
    	}
    	$exploded = $this->explode();
    	return $exploded[0] . $size . '.' . $exploded[1];
    }

}
