<?php

namespace App;

use App\Media;
use App\Http\Requsts\PostRequest;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Post extends Model
{

    /**
     * Fillable fields for a post
     *    
     * @var array
     */
    public $fillable = [
        'title',
        'body',
        'excerpt',
        'nsfw'
    ];

    protected $casts = [
        'nsfw' => 'boolean'
    ];
    

    public function createPost($request)
    {   
        $this->generateSlug();
        $this->save();
    }


    /**
     * Generate a slug for the post
     * 
     * @return string 
     */ 
    public function generateSlug()
    {
        $this->slug = str_slug($this->title);
        // $latestSlug =
        //     static::whereRaw("slug RLIKE '^{$this->slug}(-[0-9]*)?$'")
        //         ->latest('id')
        //         ->pluck('slug');

        // if($latestSlug)
        // {
        //     $pieces = explode('-', $latestSlug);
        //     $number = intval(end($pieces));
        //     $this->slug .= '-' . ($number + 1);
        // }
    }


    public function thumbnail_resposive_column()
    {
        $image = explode('.', $this->featured->path);

        $output  = '<img srcset="';
        $output .= $image[0] . '--large.' . $image[1] . ' 500w,';
        $output .= $image[0] . '--medium.' . $image[1] . ' 300w"';
        $output .= 'sizes="(min-height: 500px) 60vh, 40vh" ';
        $output .= 'src="';
        $output .= $image[0] . '--medium.' . $image[1];
        $output .= '" alt="';
        $output .= $this->title;
        $output .= '">';
        
        return $output;
    }


     public function destroyPost()
    {
        $this->delete();
    }

    
    /**
     * Fetch all the media to a post
     * 
     * @return Media
     */
    public function featured()
    {
        return $this->hasOne('App\Media');
    }


    /**
     * Fetch all the attachments of a post
     * 
     * @return Media
     */
    public function attachments()
    {
        return $this->hasMany('App\Media', 'attachment_id');
    }


    /**
     * Set the Featured image in Media
     *    
     * @param Media::id $mediaId [description]
     * @return  bool 
     */
    public function setFeatured($mediaId)
    {
        $media = Media::find($mediaId);
        $media->post_id = $this->id;
        $media->save();
    }

    public function updateFeatured($mediaId)
    {
        if($this->featured)
        {
            $media = $this->featured;
            $media->post_id = null;
            $media->save();
        }

        $media = Media::find($mediaId);
        $media->post_id = $this->id;
        $media->save();
    }


    /**
     * Set the Attachments for a post
     * 
     * @param  Array $mediaId 
     * @return bool
     */
    public function makeAttachments($mediaId)
    {
        $media = Media::find($mediaId);
        $media->attachment_id = $this->id;
        $media->save();
    }


    /**
     * Loop through the attachments array, and assign
     * 
     * @param array $attachments [description]
     */
    public function setAttachments(array $attachments)
    {
        foreach($attachments as $attachment)
        {
            $this->makeAttachments($attachment);
        }
    }


    /**
     * Get all from Media where the attachment_id is
     * equal to the post id, and where the media id
     * is chosen from the form.  If the form selected
     * the id of the media, set the attachment_id to 
     * the post id, or set it to null
     *     
     * @param  array  $ids 
     * @return bool     
     */
    public function syncAttachments(array $ids)
    {
        $attachments = Media::whereIn('id', $ids)
                            ->orWhere('attachment_id', '=', $this->id)
                            ->get();

        foreach($attachments as $attachment)
        {
            if(!in_array($attachment->id, $ids))
            {
                $attachment->attachment_id = null;
                $attachment->save();
            }else{
                $attachment->attachment_id = $this->id;
                $attachment->save();
            }
        }
    }


    /**
     * Fetch all the tags to a post
     * 
     * @return Tag 
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }


    /**
     * Sync all the tags for the post
     * 
     * @param  Tag $tags 
     */
    public function syncTags($tags) 
    {
        $this->tags()->sync((array)$tags);
    }


    /**
     * Get all the tags for the post and assign it to an array
     * 
     * @return Tag 
     */
    public function getTagListAttribute()
    {
        return $this->tags->lists('id')->toArray();
    }


    /**
     * Get all Media for the post and assign it to an array
     * 
     * @return Media
     */
    public function getMediaListAttribute()
    {
        return $this->images->lists('id')->toArray();
    }
}
