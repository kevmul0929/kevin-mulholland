CKEDITOR.stylesSet.add('my_styles', [
	{
		name: 'Row',
		element: 'div',
		attributes: {
			'class': 'row',
		}
	},
	{
		name: 'Left',
		element: 'div',
		styles: {
			'float': 'left'
		},
		attributes: {
			'class': 'float-left'
		}
	}

]);