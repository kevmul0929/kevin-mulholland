$('#tag_list').select2({
    placeholder: 'Select a tag',
    tags: true
});


function readURL(input) 
{
    if (input.files && input.files[0]) 
    {
        var reader = new FileReader();
        reader.onload = function (e) 
        {
            $('#uploaded_image')    
            .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$('input[type="submit"]').click(function(e){
    $(this).attr('disabled', true);
    $('form').submit();
});

$('#media, #featured').click(function(e){
    e.preventDefault();
    $('.Modal').addClass('show');
});
 
// new Clipboard('.copy', {
//     text: function(trigger){
//         var text = trigger.getAttribute('aria-label');
//         return '<img src="' + text + '">';
//         return text;
//     }
// });

$('.close').click(function(e){
    $('.Modal').removeClass('show');
});