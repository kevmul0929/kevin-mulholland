var windowWidth = $(window).width();
var resizeTimer;

$(window).on('resize', function() {
	clearTimeout(resizeTimer);
	resizeTimer = setTimeout(function() {
		windowWidth = $(window).width();
		console.log(windowWidth);
		scroller();
	},250);
});

scroller();

function scroller(){
	if(windowWidth >= 750)
	{
	  	$('.Wrapper').mousewheel(function(e, delta) {
	    	this.scrollLeft -= (delta * 20);
	      	e.preventDefault();
	  	});
  	}
}

$('.Navigation__dropdown').click(function(){
	$('.Navigation__toggle').toggleClass('show');
}); 