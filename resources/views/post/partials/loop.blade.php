<article class="Article__Post">
	<div class="Post">
		<div class="Post__image">
			<div class="Blocker" {{ $post->nsfw ? 'data-hidden=true' : ''}}>
				<div class="NSFW">
					<h1>NSFW</h1>
					<p>(Content may not be safe to view)</p>
					<p>If you want to view, click here</p>
				</div>
			</div>
			<a href="{{ action('PostsController@show', [$post->slug]) }}">
				{!! $post->thumbnail_resposive_column() !!}
				<!-- <img src="{{ $post->featured->size('large') }}"> -->
			</a>
		</div><!-- Post__Image -->
			
			<div class="Post__description">
				<a href="{{ action('PostsController@show', [$post->slug]) }}">
					<h2 class="Post__description_head">{{ $post->title }}</h2>
					<div class="Post__description_body">
						{!! $post->excerpt !!}
					</div>
				</a>
				@if($post->tags)
				<ul class="Tags">
					@foreach($post->tags as $tag)
						@if($tag)
						<li><a href="{{ action('TagsController@show', [$tag->id]) }}">						{{ $tag->name }}
						</a></li>
						@endif
					@endforeach
				</ul>
				@endif
			</div>
		</a>
	</div><!-- Post -->
</article>