<div class="is-flex is-flex-row">
<div class="Card__Container is-flex is-flex-1 is-flex-column">
	<div class="Card">
		<header class="Card__title">Title</header>
		<div class="Card__body">
			{!! Form::text('title', null, ['class' => 'u-full-width Input__title', 'placeholder' => 'Title']) !!}
		</div><!-- Card__body -->
	</div><!-- Card -->
	<div class="Card">
		<header class="Card__title">Excerpt</header>
		<div class="Card__body">
			{!! Form::textarea('excerpt',null,['class' => 'u-full-width Input__body']) !!}
		</div><!-- Card__body -->
	</div>


	<div class="Card">
		<header class="Card__title">Body</header>
		<div class="Card__body">
			{!! Form::textarea('body',null,['id' => 'editor', 'rows' => 20]) !!}
		</div>
	</div><!-- Card -->
</div><!-- Card__Container--column -->


<div class="Card__Container is-flex is-flex-column">
	<div class="Card">
		<header class="Card__title">Featured Image</header>
		<div class="Card__body">
			{!! Form::select('featured', $featured, null, ['class' => 'form-control']) !!}
		</div>
	</div>
	
	<div class="Card">
		<header class="Card__title">Media</header>
		<div class="Card__body">
			<a href="#" id="media" data-attribute="addMedia" class="Button is-primary">Add media</a>
		</div>
	</div>

	<div class="Card">
		<header class="Card__title">Tags</header>
		<div class="Card__body">
			{!! Form::select('tag_list[]', $tags, null, ['id' => 'tag_list', 'class' => 'form-control', 'multiple']) !!}
		</div>
	</div>

	<div class="Card">
		<header class="Card__title">Is this NSFW?</header>
		<div class="Card__body">
			<!-- Form Switch -->
			<div class="Form__group">
			    <label class="Form__switch">
			        <input type="checkbox" id="nsfw" name="nsfw" class="Form__input" {{ (isset($post) && $post->nsfw) || old('nsfw') ? 'checked' : '' }}>
			        <span class="Form__slider"></span>
			    </label>
			</div>
		</div>
	</div>

	<div class="Card">
		<header class="Card__title">Submit</header>
		<div class="Card__body is-flex">
			{!! Form::submit($submitButtonText, ['class' => 'Button is-primary is-flex-1']) !!}
		</div>
	</div>
</div><!-- Card__Container -->

		
</div>

