@extends('layout.master')

@section('content')
<div class="Wrapper">
	<div class="Container">
        <article class="Article__Post">
    <div class="Post">
        <div class="Post__image">
            <img src="/img/logo/Mulholland__logo--Ribbon.jpg">
        </div><!-- Post__Image -->
            
            <div class="Post__description">
                <h2 class="Post__description_head">Welcome!</h2>
                <div class="Post__description_body">
                    Click on any post to read more!
                </div>
            </div>
        </a>
    </div><!-- Post -->
</article>
    @if($posts)
		@foreach($posts as $post)
			@include('post.partials.loop')
		@endforeach
    @endif
</div>
@stop

@section('footer.scripts')

@stop