<article class="Article__Post">
	<div class="Post">

		<a href="{{ action('PostsController@show', [$post->slug]) }}">
			<div class="Post__image">
				<img src="{{ $post->media->size(500) }}">
				<div class="Post__description">
					<h2 class="Post__description_head">{{ $post->title }}</h2>
					<div class="Post__description_body">
						{!! $post->excerpt !!}
					</div>
					@if($post->tags)
					<ul class="Tags">
						@foreach($post->tags as $tag)
							<li><a href="{{ action('TagsController@show', [$tag->id]) }}">						{{ $tag->name }}
							</a></li>
						@endforeach
					</ul>
					@endif
				</div>
			</div><!-- Post__Image -->
		</a>
	</div><!-- Post -->
</article>