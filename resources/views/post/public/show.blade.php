@extends('layout.master')

@section('content')
<div class="Container--vertical">
	<div class="Content">
		<div class="Content__body">
			<h1 class="Post__description_head">{{ $post->title }}</h1>
			<p class="Post__description_body">{!! $post->body !!}</p>	
		</div>
		<div class="Content__media">
			<img src="{{ $post->featured->size('wide') }}">
			@if($post->attachments)
				@foreach($post->attachments as $attachment)
					<img src="{{$attachment->size('wide')}}" alt="{{$attachment->name}}">
				@endforeach
			@endif
		</div><!-- Conetent -->
	</div>
</div>
@stop