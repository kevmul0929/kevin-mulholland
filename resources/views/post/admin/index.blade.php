@extends('layout.admin')

@section('content')
<div class="Content__header">
  <h1>Posts</h1>
  <a href="{{ action('AdminPostsController@create') }}" class="Button is-primary">Upload New Image</a>
</div>

<div class="Card__Container--row">
  <div class="Card">
  <table class="Table">
    <tbody>
        <tr>
            <th class="Table__head">id</th>
            <th class="Table__head">featured image</th>
            <th class="Table__head">title</th>
            <th class="Table__head">slug</th>
            <th class="Table__head">attachments</th>
            <th class="Table__head">tags</th>
            <th class="Table__head">NSFW</th>
            <th class="Table__head">created_at</th>
            <th class="Table__head">updated_at</th>
        </tr>
  		@foreach($posts as $post)
        <tr class="Table__row--body">
            <td>{{ $post->id }}</td>
            <td>
            @if($post->featured)
              <img src="{{ $post->featured->size('small') }}">
            @endif
            </td>
            <td>{{ $post->title }}</td>
            <td>{{ $post->slug }}</td>
            <td>
              @if($post->attachments)
                {{count($post->attachments)}}
              @endif
            </td>
            <td>
              @if($post->tags)
                @foreach($post->tags as $tag)
                  {{ $tag->name }},
                @endforeach
              @endif
            </td>
            <td>
              {{$post->nsfw}}
            </td>
            <td>{{ $post->created_at }}</td>
            <td>{{ $post->updated_at }}</td>
            
            <td><a class="button-primary" href="{{ action('AdminPostsController@edit', [$post->slug]) }}">Edit</a></td>
            <td>  
            <form action="{{ action('AdminPostsController@destroy', [$post->slug]) }}" method="POST" >
              <input type="hidden" name="_method" value="DELETE">
              {{ csrf_field() }}
              <button type="submit" class="button-danger">DELETE</button>
            </form>
      </td>
        </tr>
        @endforeach
      </tbody>
  </table>
  </div>
</div><!-- Card__Container--column -->
@stop