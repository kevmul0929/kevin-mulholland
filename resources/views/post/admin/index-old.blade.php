@extends('layout.admin')

@section('content')
<div class="Main__inner">
  <h1>Posts</h1>
  <a href="{{ action('AdminPostsController@create') }}" class="Button is-primary">Upload New Image</a>
</div>


<div class="Card__Container--row">
  <div class="Card">
  <table class="Table">
    <tbody>
        <tr>
        @foreach($columns as $column)
            <th class="Table__head">{{ $column }}</th>
  			@endforeach
        </tr>
  			@foreach($posts as $post)
        <tr class="Table__row--body">
        	@foreach($columns as $column)
            <td>{{ $post->$column }}</td>
          @endforeach
            <td><a class="button-primary" href="{{ action('AdminsController@edit', [$post->slug]) }}">Edit</a></td>
            <td>  
            <form action="{{ action('AdminsController@destroy', [$post->slug]) }}" method="POST" >
              <input type="hidden" name="_method" value="DELETE">
              {{ csrf_field() }}
              <button type="submit" class="button-danger">DELETE</button>
            </form>
      </td>
        </tr>
        @endforeach
      </tbody>
  </table>
  </div>
</div><!-- Card__Container--column -->
@stop