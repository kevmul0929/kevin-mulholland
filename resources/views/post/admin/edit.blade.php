@extends('layout.admin')

@section('content')
@include('errors.list')
	{!! Form::model($post, ['method' => 'PATCH', 'action' => ['AdminPostsController@update', $post->slug], 'files' => 'true']) !!}
		@include('post.partials.post_form', ['submitButtonText' => 'Edit Post'])
		@include('media.partials.modal')
	{!! Form::close() !!}
@stop

@section('header.scripts')
<link rel="stylesheet" type="text/css" href="/css/select2.min.css">
<script src="/plugins/ckeditor/ckeditor.js"></script>
@stop

@section('footer.scripts')
@include('plugins.ckeditor.config')
@stop