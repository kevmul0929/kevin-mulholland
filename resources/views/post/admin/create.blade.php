@extends('layout.admin')

@section('content')
<div class="Main__inner">
  @include('errors.list')
	{!! Form::open(['action' => 'AdminPostsController@store', 'files' => 'true']) !!}
		@include('post.partials.post_form', ['submitButtonText' => 'Create Post'])
		@include('media.partials.modal')
	{!! Form::close() !!}
@stop
</div>

@section('header.scripts')
<link rel="stylesheet" type="text/css" href="/css/select2.min.css">
<script src="/plugins/ckeditor/ckeditor.js"></script>
@stop

@section('footer.scripts')
@include('plugins.ckeditor.config')
@stop