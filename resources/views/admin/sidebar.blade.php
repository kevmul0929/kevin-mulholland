<aside class="Sidebar is-flex is-flex-column">
	<header class="Header">
		{{ Auth::user()->first_name }}
		<div class="Header__nav">
			<a href="/auth/logout">Logout</a>
		</div>
	</header>
	<nav class="Navigation is-flex is-flex-1">
		<ul class="Navigation__list is-flex is-flex-1 is-flex-column">
			<li class="Navigation__item is-flex">
				<a href="/admin">Dashboard</a>
			</li>
			<li class="Navigation__item is-flex">
				<a href="/admin/post">Posts</a>
			</li>
			<li class="Navigation__item is-flex">
				<a href="/admin/tag">Tags</a>
			</li>
			<li class="Navigation__item is-flex">
				<a href="/admin/media">Images</a>
			</li>
		</ul>
	</nav><!-- Navigation -->
	<div class="Footer">
		<h3>Footer</h3>  
	</div>
</aside>	