<script src="{{ asset('/plugins/tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script>
tinymce.baseURL = "/plugins/tinymce/js/tinymce";
tinymce.init({
	selector: "#tinymce",
	theme: "modern",
	plugins: "code",
	toolbar: "undo redo | cut copy paste pastetext | code",
	// insert_toolbar: 'quickimage quicktatble',
	menubar: "",
	browser_spellcheck: true,
	height: 10,
	code_dialog_height: 200,
});
</script>