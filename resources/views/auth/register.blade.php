@extends('layout.auth')

@section('content')

<div class="Card">
  <div class="Card__body">
    <h1>Register</h1>
    <hr>
    @include('errors.list')
    <form method="POST" action="/auth/register">
      {!! csrf_field() !!}

      <div class="form-group">
        <label for="first_name">First Name:</label>
        <input type="text" class="form-control" name="first_name" id="first_name" value="{{ old('first_name') }}">
      </div>

      <div class="form-group">
        <label for="last_name">Last Name:</label>
        <input type="text" class="form-control" name="last_name" id="last_name" value="{{ old('last_name') }}">
      </div>

      <div class="form-group">
        <label for="username">User Name:</label>
        <input type="text" class="form-control" name="username" id="username" value="{{ old('username') }}">
      </div>

      <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}">
      </div>

      <div class="form-group">
        <label for="password">Password:</label>
        <input type="password" class="form-control" name="password" id="password" value="{{ old('password') }}">
      </div>

      <div class="form-group">
        <label for="password_confirmation">Confirm Password:</label>
        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" value="{{ old('password_confirmation') }}">
      </div>

      <div class="form-group">
        <button type="submit" class="Button is-primary">Register</button>
      </div>


    </form>
  </div>
</div>

@stop