@extends('layout.auth')

@section('content')
<div class="Card">
  <div class="Card__body">
    <h1>Login</h1>

    @include('errors.list')

    <form method="POST" action="/auth/login">

      {!! csrf_field() !!}

      <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}">
      </div>

      <div class="form-group">
        <label for="password">Password:</label>
        <input type="password" name="password" id="password" class="form-control" value="{{ old('password') }}">
      </div>

      <div class="form-group">
        <input type="checkbox" name="remember"> Remember Me
      </div>

      <div class="form-group">
        <button type="submit" class="Button is-primary">Login</button>
      </div>
    </form>
  </div>
</div>
@stop