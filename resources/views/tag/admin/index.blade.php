@extends('layout.admin')

@section('content')
<div class="Content__header">
  <h1>Tags</h1>
  <a href="{{ action('AdminTagsController@create') }}" class="Button is-primary">Upload New Image</a>
</div>
<div class="Card__Container--row">
	<div class="Card">
		<table class="Table">
		    <tbody>
		        <tr>
		            <th class="Table__head">id</th>
		            <th class="Table__head">Tag Name</th>
		            <th class="Table__head">created_at</th>
		            <th class="Table__head">updated_at</th>
		        </tr>
		  		@foreach($tags as $tag)
		        <tr class="Table__row--body">
		            <td>{{ $tag->id }}</td>
		            <td>{{ $tag->name }}</td>
		            <td>{{ $tag->created_at }}</td>
		            <td>{{ $tag->updated_at }}</td>
		            
		            <td><a class="button-primary" href="{{ action('AdminTagsController@edit', [$tag->id]) }}">Edit</a></td>
		            <td>  
		            <form action="{{ action('AdminTagsController@destroy', [$tag->id]) }}" method="POST" >
		              <input type="hidden" name="_method" value="DELETE">
		              {{ csrf_field() }}
	              <button type="submit" class="button-danger">DELETE</button>
	            </form>
	      </td>
	        </tr>
	        @endforeach
	      </tbody>
	  </table>
	</div><!-- Card -->
</div><!-- Card__Container -->
@stop