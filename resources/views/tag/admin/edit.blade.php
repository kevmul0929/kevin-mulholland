@extends('layout.admin')

@section('content')
{!! Form::model($tag, ['method' => 'PATCH', 'action' => ['AdminTagsController@update', $tag->id]]) !!}
	@include('partials.tag_form', ['submitButtonText' => 'Edit Tag'])
{!! Form::close() !!}
@stop