@extends('layout.admin')

@section('content')
<div class="Main__inner is-flex is-flex-column">
{!! Form::open(['action' => 'AdminTagsController@store', 'class' => 'is-flex is-flex-column']) !!}
	@include('tag.partials.tag_form', ['submitButtonText' => 'Create New Tag'])
{!! Form::close() !!}
</div>
@stop