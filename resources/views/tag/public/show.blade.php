@extends('layout.master')

@section('content')
	<div class="Wrapper">
		<div class="Container">
			@foreach($tag->posts()->orderBy('updated_at', 'desc')->get() as $post)
				@include('post.partials.loop')
			@endforeach
		</div>
	</div>
@stop


@section('footer.scripts')
<script src="/js/all.js"></script>
<script>
$(document).ready(function() {
	console.log('hello');
	var windowWidth = $(window).width();
	var resizeTimer;
	scroller();
	$(window).on('resize', function() {
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
			windowWidth = $(window).width();
			console.log(windowWidth);
			scroller();
		},250);
	});

	function scroller(){
		if(windowWidth >= 750)
		{
		  	$('.Wrapper').mousewheel(function(e, delta) {
		    	this.scrollLeft -= (delta * 40);
		      	e.preventDefault();
		  	});
	  	}
	}
});
</script>
@stop