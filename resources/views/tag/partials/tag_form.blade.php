<div class="Card__Container is-flex">
	<div class="Card is-flex is-flex-1 is-flex-column">
		<header class="Card__body">Create a new Tag</header>
		<div class="Card__body is-flex">
			{!! Form::text('name', null, ['class' => 'is-flex-1 Input__title', 'placeholder' => 'Name']) !!}
		</div>
	</div><!-- Card -->
	<div class="Card Flex__300px">
		<header class="Card__body">Submit</header>
		<div class="Card__body">
			{!! Form::submit($submitButtonText, ['class' => 'Button is-primary']) !!}
		</div>
	</div><!-- Card -->
</div><!-- Card__Container -->