<div class="Modal">
<div class="close">&times;</div>
<div class="Modal__inner is-flex is-flex-row space-between">
	
	@foreach($images as $image)
	<div class="Card border-none is-left">
		<div class="Form__checkbox">
			<label for="attachments[]">
				<input type="checkbox" 
					@if(isset($post) && $image->attachment_id == $post->id)
						checked 
					@endif
					value="{{$image->id}}" name="attachments[]"> {{$image->name}}
			</label>
		</div>
		<a href="#" class="copy">
			<img src="{{$image->size('medium')}}">
		</a>
	</div>

	@endforeach
</div>
</div>