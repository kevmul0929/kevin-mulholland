@extends('layout.admin')

@section('content')
<div class="Content__header">
	<h1>Media</h1>
	<a href="{{ action('AdminMediaController@create') }}" class="Button is-primary">Upload New Image</a>
</div>

<div class="Card__Container">
	<section class="Card__Container is-flex is-flex-row">
		@foreach($media as $media)
			<div class="Card">
				<img src="{{ $media->size('medium') }}" >
				<div class="Card__body">
					<p>{{ $media->name }}</p>
				</div>
				<div class="Card__body is-flex space-between">
					<a class="Button is-primary" href="{{ action('AdminMediaController@edit', [$media->id]) }}">Edit</a>
					<form action="{{ action('AdminMediaController@destroy', [$media->id]) }}" method="POST" >
						<input type="hidden" name="_method" value="DELETE">
						{{ csrf_field() }}
						<button type="submit" class="Button is-danger">DELETE</button>
		            </form>
				</div>
			</div>
		@endforeach
	</section>
</div>
@stop