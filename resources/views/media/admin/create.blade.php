@extends('layout.admin')

@section('content')
<div class="Main__inner">
	<div class="Card hidden" style="width: 400px">
		<img id="uploaded_image" src="" alt="Your uploaded image will appear here">
	</div>
</div>
<div class="Main__inner">
	{!! Form::open(['action' => 'AdminMediaController@store', 'files' => 'true']) !!}
		{!! Form::file('file', ['onchange' => 'readURL(this);']) !!}
		{!! Form::text('name', null, ['class' => 'full-width', 'placeholder' => 'Image Name']) !!}
		{!! Form::submit('Submit', ['class' => 'Button is-primary']) !!}
	{!! Form::close() !!}
</div>
@stop

@section('footer.scripts')
<script src="/js/jquery.js"></script>
<script>
	
function readURL(input) 
{
	if (input.files && input.files[0]) 
	{
		var reader = new FileReader();
		reader.onload = function (e) 
		{
			$('#uploaded_image')
			.attr('src', e.target.result);
		};
		reader.readAsDataURL(input.files[0]);
	}
}
</script>
@stop