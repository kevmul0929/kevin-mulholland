<!DOCTYPE html>
<html>
<head>
	<title>Kevin Mulholland | Auth</title>
	<link rel="stylesheet" type="text/css" href="/css/admin_styles.css">
	@yield('header.scripts')
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="Wrapper">
<div class="Container">
	@yield('content')
</div>
</div><!-- wrapper -->
<script src="/js/all.js"></script>
@yield('footer.scripts')
</body>
</html>