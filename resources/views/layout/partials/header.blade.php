@inject('tags', 'App\Tag')

<header>
    <nav class="Navigation">
        <div class="Navigation__dropdown">
        </div>
        <div class="Navigation__toggle">    
            <ul class="Navigation__list">
                <li class="Navigation__item">
                    <a href="/">Home</a>
                </li>
            </ul>
                <div class="Navigation__dropdown--bars">
                    <span class="bar"></span>
                    <span class="bar"></span>
                    <span class="bar"></span>
                </div>
        <ul class="Navigation__list">
            <li class="Navigation__item"><a href="#" class="Utility__disabled">Tags</a>
                <ul class="Navigation__Sub_List">
                    @foreach($tags->all() as $tag)
                        <li class="Navigation__item">
                            <a href="{{ action('TagsController@show', [$tag->id]) }}">
                                {{ $tag->name }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </li>
            <!-- TODO:: Make for AUTH only.  Maybe even SuperUser.  Maybe an Admin section -->
            @if(Auth::user())
            <li class="Navigation__item deactivated">
                <a href="/admin">Admin</a>
            </li>
            @endif
        </ul> 
        </div>
    </nav>
</header>