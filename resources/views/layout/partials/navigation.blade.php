@inject('tags', 'App\Tag')
<nav class="Navigation">
	<ul class="Navigation__list Navigation__list--row">
		<li><a href="/">Home</a></li>
		<li class="Navigation__dropdown">
			<div class="Navigation__dropdown--bars">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </div>
		</li>
	</ul>
	<div class="Navigation__toggle">
		<ul class="Navigation__list">
            <li class="Navigation__item"><a href="#" class="Utility__disabled">Tags</a>
                <ul class="Navigation__Sub_List">
                    @foreach($tags->all() as $tag)
                        @if(count($tag->posts)>0)
                        <li class="Navigation__item">
                            <a href="{{ action('TagsController@show', [$tag->id]) }}">
                                {{ $tag->name }} 
                            </a>
                        </li>
                        @endif
                    @endforeach
                </ul>
            </li>
            <!-- TODO:: Make for AUTH only.  Maybe even SuperUser.  Maybe an Admin section -->
            @if(Auth::user())
            <li class="Navigation__item deactivated">
                <a href="/admin">Admin</a>
            </li>
            @endif
        </ul> 
	</div>
</nav>