<!DOCTYPE html>
<html>
<head>
	<title>Kevin Mulholland</title>
	<link rel="stylesheet" type="text/css" href="/css/styles.css?abc=123">
	<script>
		document.createElement("picture");
	</script>
	<script type="text/javascript" src="/js/picturefill.min.js" async></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body class="<?php echo isset($body) ? $body : "Body";?>">
@include('js.analytics')
<header>
	@include('layout.partials.navigation')
</header>
	 
@yield('content')

<script src="/js/public.js?abc=123"></script>
<script>
$(document).ready(function() {
	scroll();
});
</script>
@yield('footer.scripts')

</body>
</html>


